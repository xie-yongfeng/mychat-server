package run;

import mapper.UserMapper;
import server.Server;
import server.ServerExit;
import server.ServerJoin;
import service.UserServiceImpl;

//运行
public class Run {
    public static void main(String[] args) {
        Run.run();
    }
    public static void run(){
        //开启服务端口
        new Thread(new Server(new UserServiceImpl())).start();
        //开启加入服务端口
        new Thread(new ServerJoin(new UserServiceImpl())).start();
        //new Thread(new ServerExit(new UserServiceImpl())).start();

    }
    public static void exit(){
        new Thread(new Server(new UserServiceImpl())).stop();
        new Thread(new ServerJoin(new UserServiceImpl())).stop();
        new Thread(new ServerExit(new UserServiceImpl())).stop();
    }
}
