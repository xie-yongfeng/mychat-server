package service;

import entity.User;
import mapper.UserMapper;
import run.Run;
import utils.MybatisUtils;

import java.util.List;

public class UserServiceImpl implements UserMapper {

    UserMapper mapper=MybatisUtils.getUserMapper();
    @Override
    public List<User> findAll() {
        try {
            return mapper.findAll();
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return null;
    }

    @Override
    public User findByPort(String port) {
        try {
            return mapper.findByPort(port);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return null;
    }

    @Override
    public List<User> findAlluserport() {
        try {
            return mapper.findAlluserport();
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return null;
    }

    @Override
    public User findByPortuserport(String port) {
        try {
            return mapper.findByPortuserport(port);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return null;
    }

    @Override
    public User findByName(String name) {
        try {
            return mapper.findByName(name);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return null;
    }

    @Override
    public boolean insert(User user) {
        try {
            return mapper.insert(user);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return false;
    }

    @Override
    public boolean insertuserport(User user) {
        try {
            return mapper.insertuserport(user);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return false;
    }

    @Override
    public boolean delete(User user) {
        try {
            return mapper.delete(user);
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return false;
    }

    @Override
    public boolean deleteuALlserport() {
        try {
            return mapper.deleteuALlserport();
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return false;
    }

    @Override
    public boolean deleteAll() {
        try {
            return mapper.deleteAll();
        } catch (Exception e) {
            MybatisUtils.sqlsessionrollback();
            //Run.run();
        }finally {
            MybatisUtils.sqlsessioncommit();
        }
        return false;
    }
}
