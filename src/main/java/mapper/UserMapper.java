package mapper;

import entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface UserMapper {
    List<User> findAll();
    User findByPort(String port);
    @Select("select * from userport")
    List<User> findAlluserport();
    @Select("select * from userport where port=#{port}")
    User findByPortuserport(String port);
    User findByName(String name);
    boolean insert(User user);
    @Insert("insert into userport values (#{port},#{name},#{serverport})")
    boolean insertuserport(User user);
    boolean delete(User user);
    @Delete("delete from userport")
    boolean deleteuALlserport();
    boolean deleteAll();
}
