package utils;


import com.ctrip.framework.apollo.core.utils.PropertiesUtil;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

public class ServerConfig {

    public static int serverConfig(String portname) {
        InputStream fis=null;
        try {
            fis = PropertiesUtil.class.getClassLoader().getResourceAsStream("serverconfig.properties");
            Properties mConfig = new Properties();
            mConfig.load(new InputStreamReader(fis, "utf-8"));
            return Integer.parseInt(mConfig.getProperty(portname));
        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            try {
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
}
