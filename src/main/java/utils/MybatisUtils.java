package utils;

import mapper.UserMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionException;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

public class MybatisUtils {
    private static SqlSessionFactory sqlSessionFactory;
    private static SqlSession session;

    static{
        // 使用mybatis获得sqlSessionFactory对象
        try {
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            session= sqlSessionFactory.openSession();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static UserMapper getUserMapper(){
        return session.getMapper(UserMapper.class);
    }
    public static void sqlsessioncommit(){
        session.commit();
    }
    public static void sqlsessionrollback(){
        session.rollback();
    }
    public static void sqlsessionclose(){
        session.close();
    }
}
